const express = require('express');
const router = express.Router();
const db = require('../src/connection');
const package_json = require('../package.json');

router.get('/', function(req, res, next) {
  String.prototype.toHHMMSS = function () {
    let sec_num = parseInt(this, 10); // don't forget the second param
    let days    = Math.floor(sec_num / 3600 / 24);
    let hours   = Math.floor(sec_num / 3600);
    let minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    let seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours   < 10) {days   = "0"+days;}
    if (hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
    if (seconds < 10) {seconds = "0"+seconds;}
    let time    = days+'d '+hours+'h '+minutes+'m '+seconds+'s';
    return time;
  }

  let time = process.uptime();
  let uptime = (time + "").toHHMMSS();

  db.query("SELECT * from connexion", function(err, rows, fields){
    if(err){
      res.status(200).send({
        running: true,
        database: "Error",
        uptime: uptime,
        version:{
          api: 'v1.0.0',
          node: process.version,
        },
        arch: process.arch,
        os: process.platform
      })
    }else{
      res.status(200).send({
        running: true,
        database: "OK",
        uptime: uptime,
        version:{
          api: 'v1.0.0',
          node: process.version,
        },
        arch: process.arch,
        os: process.platform
      })
    }
  })
});

module.exports = router;
