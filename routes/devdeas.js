const express = require('express');
const router = express.Router();
const db = require('../src/connection');
const auth = require('../src/routeAuth');

router.get('/', function(req, res, next) {
  res.render('index', { title: 'Devdeas' });
});


/*
* ALL GET ROUTES RELATED TO DEVDEAS
*/
router.get('/all', function(req, res){
  db.query('SELECT * FROM devdeas ORDER BY id DESC', function(err, rows, fields){
    if(err){
      res.status(400).send({
        'error': err.message
      });
      console.log(err.message);
    }else{
      res.status(200).send(rows);
    }
  });
});

router.get('/certified', function(req, res){
  db.query('SELECT * FROM devdeas WHERE certified = 1', function(err, rows, fileds){
    if(err){
      res.status(400);
      console.log(err.message);
    }else{
      res.status(200).send(rows);
    }
  });
});

router.get('/uncertified', function(req, res){
  db.query('SELECT * FROM devdeas WHERE certified = 0', function(err, rows, fields){
    if(err){
      res.status(400);
      console.log(err.message);
    }else{
      res.status(200).send(rows);
    }
  });
});

/*
* ALL POST ROUTES RELATED TO DEVDEAS
*/

router.post('/add', function(req, res){

  if(!auth.authorize(req, res)){
    console.log(" => Error - 401 - Unauthorized");
    return;
  }

  let values = [
    [req.body.title, req.body.author, req.body.description, res.body.language, req.body.keywords, req.body.image]
  ];
  let sql_request = 'INSERT INTO devdeas (name, author, description, lang, keywords, img) VALUES ?';

  db.query(sql, [values], function(err, result){
    if(err){
      res.status(400).send({
        status: 400,
        error: err.message
      });
      console.log(err.message);
    }else{
      res.status(200).send(results);
    }
  });

});

router.post('/delete', function(req, res){
  db.query(`DELETE FROM devdeas WHERE name = ${req.body.title}`, function(err, result){
    if(err){
      console.log(err.message);
      res.status(400).send({
        status: 400,
        error: err.message
      });
    }else{
      res.status(200).send(results);
    }
  })
});

router.post('/update', function(req, res){
  let form = req.body; // Form response
  let columns = []; // Columns name for our SQL request
  let values = []; // Values for SQL request
  let sqlColumns; // Columns passed to sql request joined with commas
  let sqlValues; // Values passed to sql request joined with commas

  // For loop to get columns names and columns values at the same time from the response body
  for (var key in form) {
    if(form[key] == '' || form[key] == ""){
      console.log(key, 'empty');
    }else{
      console.log(key, form[key]);
      columns.push(key);
      values.push(form[key])
    }
  }

  // Remove first element because it's just a marker to know what line update in our db
  columns.shift();
  values.shift();

  // Making a new array map with our columns and corresponding values
  let sqlString = []; // Our final array with our request informations
  for (var i = 0; i < columns.length; i++) {
    sqlString[i] = columns[i] + "='" + values[i] + "'";
  }

  let sql = `UPDATE users SET ${sqlString.toString()} WHERE name = '${req.body.toUpdate.split(' ')[0]}'`;
  db.query(sql, function(err, result){
    if(err){
      console.log(err);
      res.status(400).send({
        status: 400,
        error: err.message
      });
    }else{
      res.status(200).send(results);
    }
  });
});

router.post('/certify/:id', function(req, res){
  db.query(`UPDATE devdeas SET certified = 1 WHERE id = ${req.params.id}`, function(err, result){
    if(err){
      console.log(err.message);
    }else{
      console.log(result);
    }
  });
});

router.post('/uncertify/:id', function(req, res){
  db.query(`UPDATE devdeas SET certified = 0 WHERE id = ${req.params.id}`, function(err, result){
    if(err){
      console.log(err.message);
    }else{
      console.log(result);
    }
  });
});

router.post('/setActive/:id', function(req, res){
  db.query(`UPDATE devdeas SET active = 1 WHERE id = ${req.params.id}`, function(err, result){
    if(err){
      console.log(err.message);
    }else{
      console.log(result);
    }
  });
});

router.post('/setInactive/:id', function(req, res){
  db.query(`UPDATE devdeas SET active = 0 WHERE id = ${req.params.id}`, function(err, result){
    if(err){
      console.log(err.message);
    }else{
      console.log(result);
    }
  });
});

module.exports = router;
