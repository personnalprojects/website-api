const express = require('express');
const router = express.Router();
const util = require('minecraft-server-util');

router.get('/', function(req, res, next) {
  util.status('185.163.124.93')
  .then((response) => {
    res.send(response)
  })
  .catch((error) => {
    console.error(error);
  });
});

module.exports = router;
