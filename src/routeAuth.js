const config = require('../config/config');

function authorize(request, response){
  let token = request.query.token;

  if(token != config.TOKEN){
    res.status(401).end();
    return false;
  }else{
    return true;
  }
}

module.exports = authorize;
