const mysql = require('mysql');
const settings = require('../config/db.config.js');
const boxen = require('boxen');
let db;

function connectToDatabase(){
  if(!db){
    db = mysql.createConnection({
      host: settings.HOST,
      port: settings.PORT,
      user: settings.USER,
      password: settings.PASS,
      database: settings.DATABASE
    });

    db.connect(function(err){
      if(!err){
        console.log('\x1b[0m\x1b[37m >> Database connexion : \x1b[1m\x1b[4m\x1b[32mConnected\x1b[0m\n');
        // console.log('\x1b[0m\x1b[37m >> Connected to : \x1b[1m\x1b[4m\x1b[32mError\x1b[0m\n');
      }else{
        console.log('\x1b[0m\x1b[37m >> Database connexion : \x1b[1m\x1b[4m\x1b[31mError\x1b[0m\n');
        console.log(err);
      }
    });
  }

  return db;
}

module.exports = connectToDatabase();
